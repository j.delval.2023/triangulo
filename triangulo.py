import sys


def line(number):
  return str(number) * number


def triangle(number):
  if number < 1 or number > 9:
    raise ValueError("Please provide a number (between 1 and 9) as argument.")

  triangle_str = ""
  for i in range(1, number + 1):
    triangle_str += line(i) + "\n"
  return triangle_str


def main():
  if len(sys.argv) != 2:
    print("Please provide a number (between 1 and 9) as argument.")
    return

  try:
    number = int(sys.argv[1])
  except ValueError:
    print("Please provide a number (between 1 and 9) as argument.")
    return

  try:
    print(triangle(number))
  except ValueError as e:
    print(e)
  except:
    print(triangle(9))


if __name__ == "__main__":
  main()




